import React from 'react';

import Vert from '../scripts/animations/verts';
import Dust from '../scripts/animations/dust';
import '../style/gradients.css';

let Anims = {
	vert: Vert,
	dust: Dust
}

class Background extends React.Component {
	componentDidMount(){
		if(this.props.anim) Anims[this.props.anim](this.refs.anim, this.props.style);
	}

	render() {
		let image = this.props.image ? <img key="1" src={this.props.image} className="layer" alt="none"/> : null;
		let anim = this.props.anim ? <canvas key="2" ref="anim" className="layer"/> : null;
		let gradient = this.props.grad ? <div key="3" className={this.props.grad + " layer"}/> : null;

		return [anim, image, gradient, this.props.children];
	}
}

export default Background;