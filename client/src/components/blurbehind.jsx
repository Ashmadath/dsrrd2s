import React from 'react';
import html2canvas from 'html2canvas';
import StackBlur from 'stackblur-canvas';

class BlurBehind extends React.Component{
	constructor(props){
		super(props);
		this.state = {rendered: false}

		this.state.posstyle = {
			position: props.style.position,
			left: props.style.left,
			top: props.style.top,
			width: props.style.width,
			height: props.style.height
		}
	}

	componentDidMount(){
		html2canvas(document.body).then((canvas) => {
			this.refs.container.appendChild(canvas);

			let context = canvas.getContext("2d");
			let rect = this.refs.container.getBoundingClientRect();
			let cvsRect = canvas.getBoundingClientRect();

			StackBlur.canvasRGBA(canvas, cvsRect.x, cvsRect.y, cvsRect.width, cvsRect.height, 7);

			canvas.imagedata = context.getImageData(rect.x, rect.y, rect.width,rect.height);
			context.clearRect(0, 0, cvsRect.width, cvsRect.height);
			context.putImageData(canvas.imagedata, rect.x, rect.y, 0,0,rect.width,rect.height);

			canvas.style.position = 'fixed';
			canvas.style.left = '0px';
			canvas.style.top = '0px';
			canvas.style.zIndex = -1;

			this.setState({rendered: true});
		});
	}

	render(){
		return(
			<div ref="container" style={this.state.posstyle}>
				<div style={this.props.style}>
					{this.state.rendered ? this.props.children : null}
				</div>
			</div>
		);
	}
}

export default BlurBehind;