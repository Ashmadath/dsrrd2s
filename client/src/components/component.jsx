import React from 'react';

class Ext extends React.Component{
	constructor(props){
		super(props);

		this.state = {style: {}};

		if(this.props.lift)
			this.props.lift.ref = this;

		if(this.props.layer)
			this.state.style = Object.assign({
				position: 'fixed',
				left: 0,
				top: 0,
				width: '100vw',
				height: '100vh'
			}, this.state.style);

		if(this.props.full)
			this.state.style = Object.assign({
				width: '100vw',
				height: '100vh',
				display: 'inline-block'
			}, this.state.style);

		if(this.props.margin)
			this.state.style = Object.assign({
				margin: this.props.vertical ? 'auto 0' : 
						this.props.horizontal ? '0 auto' : 'auto' 
			}, this.state.style);
	
		this.state.style = Object.assign(this.props.style || {}, this.state.style);
}

	render(){
		return(
			<div 
				className={this.props.className} 
				style={this.state.style} 
				onTransitionEnd={this.props.onTransitionEnd}
				ref="elem">
				{this.props.children}
			</div>
		);
	}
}

class Row extends Ext{
	constructor(props){
		super(props);

		this.state.style = Object.assign({
			display: 'flex',
			flexDirection: 'row',
			justifyContent: 'center',
			alignItems: 'stretch'
		}, this.state.style);

		this.state.style = Object.assign(this.props.style || {}, this.state.style);
	}
}

class Col extends Ext{
	constructor(props){
		super(props);

		this.state.style = Object.assign({
			display: 'flex',
			flexDirection: 'column',
			justifyContent: 'center',
			alignItems: 'stretch',
			textAlign: 'center'
		}, this.state.style);

		this.state.style = Object.assign(this.props.style || {}, this.state.style);
	}
}

class Layer extends Ext{
	constructor(props){
		super(props);

		this.state.style = Object.assign({
			position: 'fixed',
			left: 0,
			top: 0,
			width: '100vw',
			height: '100vh',
			textAlign: 'center'
		}, this.state.style);

		this.state.style = Object.assign(this.props.style || {}, this.state.style);
	}
}

class Full extends Ext{
	constructor(props){
		super(props);		
		this.state.style = Object.assign({
			width: '100vw',
			height: '100vh',
			display: 'inline-block'
		}, this.state.style);

		this.state.style = Object.assign(this.props.style || {}, this.state.style);
	}
}

export { Col, Row, Ext, Layer, Full};