import React from "react";
import { Ext } from './component';

class Switch extends React.Component {
	constructor(props) {
		super(props);

		this.Switch = this.Switch.bind(this);
		this.state = Object.assign(this.state, {shown: 0});
	}

	render() {
		return this.props.children[this.state.shown];
	}

	Switch(target) {
		this.setState({ shown: target });
	}
}

class FlagSwitch extends Switch{
	render(){
		return this.props.children.map((child,index) => {
			<MountAnim mounted={this.state.show == index}>
				{child}
			</MountAnim>
		});
	}
}

class MountAnim extends React.Component{
	constructor(){
		super(props);

		this.state = {
			current: {}, 
			show: false
		}
	}

	componentWillRecieveProps(props){
		if(!props.mounted) this.setState({current: this.props.out});
		else{
			this.setState({show: true})
			setInterval(() => this.setState({current: this.props.in}), 10);
		}
	}

	transitionEnd(){
		this.setState({show: this.props.mounted})
	}

	render(){
		if(!this.state.show) return null;

		if(React.isValidElement(this.props.child) && !this.props.container)
			return React.cloneElement(this.props.children, {
					style: this.state.current,
					onTransitionEnd: this.transitionEnd() 
				});
		else 
			return (
				<div onTransitionEnd={this.transitionEnd()} style={this.state.current}>
					{this.props.children}
				</div>
			);

	}
}

export { Switch, FlagSwitch, MountAnim };
