import React from 'react';
import { render } from 'react-dom';
import {BrowserRouter as Router, Route} from 'react-router-dom';

import './styles/reset';
import './styles/architecture';
import Index from './pages/index.jsx';

let App = () => {
	return (
		<Router>
			<div>
				<Route path="/" component={Index}/>
			</div> 
		</Router>
	);
}


render(<App/>, document.body);