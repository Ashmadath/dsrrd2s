import React from "react";
import {Switch, Route} from 'react-router-dom';

import { Col, Row, Ext } from "../components/component";
import Librarium from "./librarium";
import Arnehel from "./arnehel";
import Development from "./development";

import Shaded from "../scripts/three/shaded";
import Noise from "../scripts/three/noise";

let Routes = () => {
	return (
		<Switch>
			<Route path="/lib" component={Librarium}/>
			<Route path="/arh" component={Arnehel}/>
			<Route path="/dev" component={Development}/>
			<Route path="/" component={Index}/>
		</Switch>
	);
}


class Index extends React.Component{
	componentDidMount(){
		let shadedCanvas = new Shaded(this.refs.noise);
		shadedCanvas.Add(new Noise());
	}

	render(){
		return (
			<div>
				<div className="layer" ref="noise"/>
				<Col layer style={{background: '#000000CC'}}>
					<code> This area is a work in progress </code>
					<code> Website currently in use is at <a style={{display: "inline", color: "#9999FF"}} href="http://andrija.net">andrija.net</a> </code>
					<code> Repository of this website can be found at <a style={{display: "inline", color: "#9999FF"}} href="https://bitbucket.org/Ashmadath/dsrrd2s/">https://bitbucket.org/Ashmadath/dsrrd2s/</a></code>
					<div style={{fontSize: "4vmin", lineHeight: "5vmin"}}>Andrija Kovač</div>
					<Row style={{display: "none"}}>
						<a href="/lib" className="button_pane"><img alt="fuck alt" src="/img/icon/astar.svg"/><code> Librarium </code></a>				
						<a href="/arh" className="button_pane"><img alt="fuck alt" src="/img/icon/astar.svg"/><code> Arnehel </code></a>				
						<a href="/dev" className="button_pane"><img alt="fuck alt" src="/img/icon/astar.svg"/><code> Development Sim </code></a>				
					</Row>
				</Col>
			</div>
		);
	}
};

export default Routes;
