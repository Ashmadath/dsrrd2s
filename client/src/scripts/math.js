function DMath() {}

DMath.Distance = (a, b) => {
	return Math.sqrt(DMath.Distance2(a, b));
};

DMath.Distance2 = (a, b) => {
	let x = a.x - b.x;
	let y = a.y - b.y;

	return x * x + y * y;
};

DMath.Random = {};

DMath.Random.Float = (min, max) => {
	return min + Math.random() * (max - min);
};

DMath.Random.Int = (min, max) => {
	return Math.floor(DMath.Random.Float(min, max));
};

DMath.Jump = (current, min, max, border) => {
	current = current < min - border ? max + border : current;
	current = current > max + border ? min - border : current;
	return current;
};

export default DMath;