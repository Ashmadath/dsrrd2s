import * as THREE from 'three';
import Options from '../utilities/options';
import DMath from '../utilities/math';

function Verts(container, style){
	let size = container.getBoundingClientRect();

	style = Verts.Style.Validate(style);

	const camera = new THREE.OrthographicCamera(size.width / -2,size.width / 2, size.height / 2, size.height / -2,0.1, 100000);
	const renderer = new THREE.WebGLRenderer();
	const scene = new THREE.Scene();

	scene.add(camera);
	renderer.setSize(size.width, size.height);
	container.appendChild(renderer.domElement);

	let uniforms = {
		time: {type: 'f', value: 1.0},
		speed: {type: 'f', value: style.speed},
		color: {type: 'v4', value: new THREE.Vector4(1,1,1,1)},
	}

	let material = new THREE.ShaderMaterial({
		defines: { USE_COLOR: true },
		uniforms: uniforms,
		vertexShader: Vert,
		fragmentShader: Frag,
		wireframe: true
	});

	let geometry = new THREE.PlaneGeometry(600,600, style.gridsize, style.gridsize);

	console.log(geometry);

	for(let i in geometry.vertices)
		geometry.colors[i] = new THREE.Color(DMath.Random.Float(0,255), DMath.Random.Float(0,255), 0, 1);
	geometry.colorsNeedUpdate = true;

	let mesh = new THREE.Points(geometry, material);

	mesh.position.z = -600;
	scene.add(mesh);

	let starttime = Date.now();

	setInterval(() => {
		uniforms.time.value = Date.now() - starttime;

		renderer.render(scene, camera); 
	}, 60);
}

Verts.Style = new Options({
	gridsize: 10,
	transparent: false,
	fill: '#000000',
	dot: '#FFFFFF',
	border: 50,
	thickness: 2,
	distance: 150,
	alpha_mult: 1,
	line: 'rgba(255,255,255,',
	speed: 0.1
});

let Vert = `
	uniform float time;
	uniform float speed;

	void main(){
		vec4 pos = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );
		vec3 vel = color;
		vel.r = ((vel.r / 127.0) - 1.0) * speed;
		vel.g = ((vel.g / 127.0) - 1.0) * speed;

		pos.x = (pos.x + 1.0) / 2.0;
		pos.y = (pos.y + 1.0) / 2.0;

		pos.x = mod(pos.x + time * vel.r / 1000.0, 1.0);
		pos.y = mod(pos.y + time * vel.g / 1000.0, 1.0);

		pos.x = pos.x * 2.0 - 1.0;
		pos.y = pos.y * 2.0 - 1.0;

		gl_Position = pos;
	}

`;

let Frag = `

	void main(){
		gl_FragColor = vec4(1,1,1,1);
	}

`;

export default Verts;