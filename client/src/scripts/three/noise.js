function Noise() {
	this.vert = vert;
	this.frag = frag;
	this.uniforms = {
		time: { type: 'f', value: 0.99}
	}

	this.update = () => {
		this.uniforms.time.value = (Date.now() - start) / 1000;
	}

	let start = Date.now();
}

let vert = `
	varying vec2 vUv;

	void main(){
		vUv = uv;
		gl_Position = projectionMatrix * modelViewMatrix * vec4(position, 1.0 );
	}
`

let frag = `
	uniform float time;
	varying vec2 vUv;

	void main(){
		vec2 vec = vUv + vec2(time,time);
		vec.x = mod(vec.x, 1.0);
		vec.y = mod(vec.y, 1.0);
		float a = fract(sin(dot(vec ,vec2(12.9898,78.233))) * 43758.5453);
		gl_FragColor = vec4(a / 2.0,a / 2.0,a / 1.9,0.2);
	}
`

export default Noise;