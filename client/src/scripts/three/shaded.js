import * as THREE from 'three';

function Shaded(container, style){
	const size = container.getBoundingClientRect();
	const camera = new THREE.OrthographicCamera(size.width / -2, size.width / 2, size.height / 2, size.height / -2, 0.1, 100000);
	const renderer = new THREE.WebGLRenderer();
	const scene = new THREE.Scene();

	scene.add(camera);
	renderer.setSize(size.width, size.height);
	container.appendChild(renderer.domElement);

	const shaders = [];
	const interval = setInterval(() => {
		for(let s of shaders)
			s.update();

		renderer.render(scene,camera);
	}, 16);

	this.Add = (shader) => {
		let material = new THREE.ShaderMaterial({
			uniforms: shader.uniforms,
			vertexShader: shader.vert,
			fragmentShader: shader.frag
		});
		console.log(size);
		console.log(shader);
		let plane = new THREE.PlaneGeometry(size.width,size.height);
		let mesh = new THREE.Mesh(plane, material);
		mesh.position.z = -1000;

		scene.add(mesh);
		shaders.push(shader);
	}
}

export default Shaded;