import DMath from '../utilities/math.js';
import Options from '../utilities/options.js';

function Vertices(canvas, style) {
	style = Vertices.Style.Validate(style);

	let size = Vertices.Resize(canvas);
	canvas.onresize += () => size = Vertices.Resize(canvas);
	let context = canvas.getContext('2d');

	let vertCount = size.w * size.h / (style.gridsize * style.gridsize);
	let verts = [];

	let Vert = function() {
		let self = this;

		this.x = DMath.Random.Int(0, size.w);
		this.y = DMath.Random.Int(0, size.h);
		this.vx = DMath.Random.Float(-1, 1);
		this.vy = DMath.Random.Float(-1, 1);
		this.t = style.thickness;

		this.Update = () => {
			self.x = DMath.Jump(self.x + self.vx, 0, size.w, style.border);
			self.y = DMath.Jump(self.y + self.vy, 0, size.h, style.border);
		};

		this.Draw = () => {
			context.beginPath();
			context.arc(self.x, self.y, self.t, 0, Math.PI * 2, false);
			context.fill();
		};
	};

	for (let i = 0; i < vertCount; i++)
		verts[i] = new Vert(style);	

	let Update = () => {
		context.clearRect(0, 0, size.w, size.h);
			
		if (!style.transparent){
			context.fillStyle = style.fill;
			context.fillRect(0, 0, size.w, size.h);
		}

		context.fillStyle = style.dot;

		for (let i = 0; i < verts.length; i++) {
			let vert = verts[i];
			vert.Update();
			vert.Draw();

			for (let j = i + 1; j < verts.length; j++) {
				let pair = verts[j];
				let distance = DMath.Distance(vert, pair);

				if (distance < style.distance) {
					let alpha = 1 - distance / style.distance;
					alpha *= style.alpha_mult
					alpha = Math.floor(alpha * 255);

					context.beginPath();
					context.strokeStyle = style.linecolor + alpha.toString(16);
				
					context.moveTo(vert.x, vert.y);
					context.lineTo(pair.x, pair.y);
					context.stroke();
					context.closePath();
				}
			}
		}
	};
	setInterval(Update, 16);
}

Vertices.Style = new Options({
	gridsize: 100,
	transparent: false,
	fill: '#000000',
	dot: '#FFFFFF',
	border: 50,
	thickness: 2,
	distance: 150,
	linecolor: '#FFFFFF',
	linealpha: 1,
});


Vertices.Resize = (canvas) => {
	canvas.width = canvas.clientWidth;
	canvas.height = canvas.clientHeight;

	return {
		w: canvas.width,
		h: canvas.height
	};
};

export default Vertices;