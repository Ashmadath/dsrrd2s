const express = require('express');
const path = require('path');

const PORT = 80;

const server = express();

server.use(express.static(path.resolve('client/public')));
server.use('/', (req,res) => res.sendFile(path.resolve('client/public/index.html')));

server.listen(PORT);
console.log('Server listening on port: ' + PORT);