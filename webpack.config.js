const webpack = require('webpack');
const path = require('path');

const BUILD_DIR = path.resolve('./client/public/build');
const APP_DIR = path.resolve('./client/src');

const config = {
	entry: {
		main: APP_DIR + '/index.jsx'
	},
	output: {
		filename: 'bundle.js',
		path: BUILD_DIR,
	},
	resolve: {
		extensions: ['.js', '.jsx', '.css']
	},
	module:{
		rules: [
			{
				test: /\.css$/,
				use: [
					{ loader: 'style-loader'},
					{ loader: 'css-loader'}
				]
			},
			{
				test: /\.jsx?$/,
				use:[{
					loader: 'babel-loader',
					options: {
						presets: ['es2015', 'react', 'stage-2']
					}
				}],
				exclude: /node_modules/
			}
		]
	},
	mode: "development"
}

module.exports = config;